var APP_DATA = {
  "scenes": [
    {
      "id": "0-49449075608_6e38cceebf_k",
      "name": "49449075608_6e38cceebf_k",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        }
      ],
      "faceSize": 512,
      "initialViewParameters": {
        "yaw": 0.8846442459548722,
        "pitch": -0.12326996741450635,
        "fov": 1.4134061960355204
      },
      "linkHotspots": [],
      "infoHotspots": []
    },
    {
      "id": "1-49449783032_3f1df95ab2_c",
      "name": "49449783032_3f1df95ab2_c",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        }
      ],
      "faceSize": 200,
      "initialViewParameters": {
        "yaw": 0.8995158452586125,
        "pitch": 0.12314462306161289,
        "fov": 1.4134061960355204
      },
      "linkHotspots": [],
      "infoHotspots": []
    }
  ],
  "name": "Project Title",
  "settings": {
    "mouseViewMode": "drag",
    "autorotateEnabled": true,
    "fullscreenButton": false,
    "viewControlButtons": false
  }
};
